const { entries, assets, pages } = require("../../util/cms")

module.exports = {
	all: async () => [{}],

	permalink: "/site/",

	data: async ({ request, data }) => {
		return {
			pages,
		}
	},

	// template: 'Contentful.svelte' // this is auto-detected.
	// layout: 'Layout.svelte' // this is auto-detected.
}
