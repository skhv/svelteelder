const { entries, assets, pages } = require("../../util/cms")

module.exports = {
	all: async () =>
		pages.map((page) => ({
			slug: page.slug,
			id: page.id,
		})),

	permalink: "/site/:slug/",

	data: async ({ request, data }) => {
		const { id } = request
		return {
			...data,
			...pages.find((entry) => entry.id === id).fields,
		}
	},

	// template: 'Contentful.svelte' // this is auto-detected.
	// layout: 'Layout.svelte' // this is auto-detected.
}
