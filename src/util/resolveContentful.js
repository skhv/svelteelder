module.exports = function resolve(data) {
	const result = cloneDeep(data)

	// add content type and id to fields
	Object.values(result).forEach((category) => {
		Object.values(category).forEach((item) => {
			if (item.fields) {
				if (item.id && !item.fields.id) {
					item.fields.id = item.id
				}
				if (item.contentType && !item.fields.contentType) {
					item.fields.contentType = item.contentType
				}
			}
		})
	})
	walk(result.entries, (obj) => {
		if (!obj) return false
		if (obj.sys && obj.sys.type === "Link") {
			const source =
				obj.sys.linkType === "Entry" ? result.entries : result.assets
			return source[obj.sys.id]
		}
		return false
	})
	return result
}

function walk(val, resolver) {
	if (!val) return
	if (val instanceof Array) {
		let missing = false
		for (let i = 0; i < val.length; i++) {
			if (typeof val[i] === "object") {
				const resolved = resolver(val[i])
				if (resolved !== false) {
					if (!val[i]) missing = true
					val[i] = resolved
				} else {
					walk(val[i], resolver)
				}
			}
		}
		if (missing) {
			console.log("Missing items")
			for (let i = val.length - 1; i >= 0; i--) {
				if (typeof val[i] === "undefined") {
					val.splice(i, 1)
				}
			}
		}
	} else {
		Object.keys(val).forEach((i) => {
			if (typeof val[i] === "object") {
				const resolved = resolver(val[i])
				if (resolved !== false) {
					val[i] = resolved
				} else {
					walk(val[i], resolver)
				}
			}
		})
	}
}

function cloneDeep(val) {
	if (!val) return val
	if (val instanceof Array) return val.map((v) => cloneDeep(v))
	if (typeof val === "object") {
		const result = {}
		for (var key in val) {
			result[key] = cloneDeep(val[key])
		}
		return result
	}
	return val
}
