const contentful = require("../../data/contentful.json")
const resolveContentful = require("./resolveContentful")
const data = resolveContentful(contentful.de)
const entries = Object.values(data.entries)
const config = entries.find(
	(entry) => entry.contentType === "websiteConfiguration"
)
const pages = entries
	.filter((entry) => entry.contentType === "webpage")
	.map((page) => ({
		...page,
		slug: getWebpageSlug(page),
	}))

module.exports = { ...data, pages }

function getWebpageSlug(page) {
	let slug = []

	let parent = page
	while (parent && parent.fields) {
		slug.unshift((parent.fields.seoSlug || "").trim())
		parent = parent.fields.parentPage
	}

	// only consider pages with home as root
	if (slug[0] === "/") {
		slug[0] = ""
	}
	slug = slug.filter(Boolean)

	return slug.join("/")
}
